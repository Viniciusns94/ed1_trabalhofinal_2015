/*-----------------------------------------------------------
 *       UNIFAL � Universidade Federal de Alfenas.        
 * Trabalho..: Editor de Legendas usando lista encadeada    
 * Disciplina: Estrutura de Dados I                         
 * Professor.: Luiz Eduardo da Silva                        
 * Aluno(s)..: Marcelo C�zar Costa                             
 *             Rom�rio da Silva Borges
 *             Thiago Mendes Meloto
 *             Vin�cius Nogueira da Silva       
 * Data......: 06/05/2015                                   
 *------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct no {
    char texto[1000];
    int horasIni, minutosIni, segundosIni, milissegundosIni;
    int horasFim, minutosFim, segundosFim, milissegundosFim;
    struct no *prox;
} *legenda;

legenda insereNo(legenda L, struct no *info) {//Insere um n� no final da lista.

    legenda aux;
    if (!L) {
        L = info;
        L->prox = NULL;
    } else {
        aux = L;

        while (L->prox)
            L = L->prox;

        L->prox = info;
        L = aux;
    }

    return L;
}

legenda criaLista(char *entrada) {

    FILE *arq;
    arq = fopen(entrada, "r+");
    char ch[5];
    int i = 0;
    legenda L, aux;
    L = NULL;

    if (arq == NULL) {
        FILE *arq = fopen(entrada, "w");
        printf("Um arquivo com o nome '%s' foi criado.", entrada);
    } else {
        fgets(ch, sizeof (ch), arq);
        while (!feof(arq)) {
            aux = (legenda) malloc(sizeof (struct no));
            if (!aux) printf("\nMem�ria Cheia");
            fscanf(arq, "%d:%d:%d,%d --> %d:%d:%d,%d\n", &aux->horasIni, &aux->minutosIni, &aux->segundosIni, &aux->milissegundosIni, &aux->horasFim, &aux->minutosFim, &aux->segundosFim, &aux->milissegundosFim);

            while (((aux->texto[i] = fgetc(arq)) != '\n') || (aux->texto[i - 1] != '\n')) {
                i++;
                if (aux->texto[i] == EOF || aux->texto[i - 1] == EOF) break;
            }

            aux->texto[i - 1] = '\0';
            aux->prox = NULL;

            L = insereNo(L, aux);
            i = 0;

            fgets(ch, sizeof (ch), arq);
        }
    }

    fclose(arq);

    return L;
}

legenda insere(legenda L) {
    int numLeg, numLeg2, totalLegendas; //numLeg->n�mero da legenda a ser inserida; numLeg2->n�mero da legenda do n� atual; 
    //totalLegendas->quantidade total de n�s que a lista possui.
    double tempoInicial, tempoFinal, tempoInicial2, tempoFinal2;
    //tempoInicial->Tempo inicial da legenda a ser inclu�da; tempoFinal->Tempo final da legenda a ser inclu�da
    //tempoInicial2->Tempo inicial do n� atual; tempoFinal2->Tempo final do n� atual;
    legenda aux = (legenda) malloc(sizeof (struct no)), comeco, atual, noAnt;
    //aux->recebe as informa��es da legenda a ser inclu�da.
    //comeco->guarda para onde aponta o comeco da lista
    //atual->guarda o endereco do no atual da lista.
    //noAnt->n� imediatamente anterior ao atual

    printf("Digite o texto da legenda que deseja incluir:\n");
    scanf(" %[A-Z a-z]", aux->texto);
    printf("Digite o n�mero da legenda que deseja inserir:\n");
    scanf("%d", &numLeg);
    printf("Digite a hora do momento INICIAL da legenda que deseja inserir:\n");
    scanf("%d", &aux->horasIni);
    printf("Digite os minutos do momento INICIAL da legenda que deseja inserir:\n");
    scanf("%d", &aux->minutosIni);
    printf("Digite os segundos do momento INICIAL da legenda que deseja inserir:\n");
    scanf("%d", &aux->segundosIni);
    printf("Digite os milissegundos do momento INICIAL da legenda que deseja inserir:\n");
    scanf("%d", &aux->milissegundosIni);
    printf("Digite a hora do momento FINAL da legenda que deseja inserir:\n");
    scanf("%d", &aux->horasFim);
    printf("Digite os minutos do momento FINAL da legenda que deseja inserir:\n");
    scanf("%d", &aux->minutosFim);
    printf("Digite os segundos do momento FINAL da legenda que deseja inserir:\n");
    scanf("%d", &aux->segundosFim);
    printf("Digite os milissegundos do momento FINAL da legenda que deseja inserir:\n");
    scanf("%d", &aux->milissegundosFim);

    tempoInicial = aux->horasIni * 3600000 + aux->minutosIni * 60000 + aux->segundosIni * 1000 + aux->milissegundosIni; //Converte todo o tempo para milissegundos.
    tempoFinal = aux->horasFim * 3600000 + aux->minutosFim * 60000 + aux->segundosFim * 1000 + aux->milissegundosFim;

    //Calcula o n�mero de n�s da lista, atribuindo o valor � vari�vel TotalLegendas.
    comeco = L;
    for (totalLegendas = 0; L; totalLegendas++) {
        L = L->prox;
    }
    L = comeco;

    if (numLeg > (totalLegendas + 1)) {
        printf("N�o � poss�vel inserir uma legenda com esse n�mero, pois o arquivo original possui apenas %d legendas.\n", totalLegendas);
        free(aux);
    }else if(totalLegendas==0){ 
        insereNo(L, aux);
    }else if (numLeg == (totalLegendas + 1)) {
        //O pr�ximo bloco alcan�ar� o �ltimo n� da lista e "atual" apontar� para ele.     
        comeco = L;
        while (L->prox)
            L = L->prox;
        atual = L;
        L = comeco;

        tempoFinal2 = atual->horasFim * 3600000 + atual->minutosFim * 60000 + atual->segundosFim * 1000 + atual->milissegundosFim;
        if (tempoFinal2 <= tempoInicial && tempoInicial < tempoFinal) {
            L = insereNo(L, aux);
            printf("Legenda inclu�da com sucesso!");
        } else {
            printf("N�o foi poss�vel inserir essa legenda, pois o tempo n�o condiz com o n�mero da legenda.\n");
            free(aux);
        }
    } else if (numLeg == 1) {
        tempoInicial = L->horasIni * 3600000 + L->minutosIni * 60000 + L->segundosIni * 1000 + L->milissegundosIni;
        tempoFinal2 = aux->horasFim * 3600000 + aux->minutosFim * 60000 + aux->segundosFim * 1000 + aux->milissegundosFim;
        if (tempoFinal2 <= tempoInicial) {
            aux->prox = L;
            L = aux;
        } else {
            printf("N�o foi poss�vel inserir essa legenda, pois o tempo n�o condiz com o n�mero da legenda.\n");
            free(aux);
        }
    } else {
        //O proximo bloco de c�digo percorre a lista at� encontrar o n� anterior �quele que se deseja inserir.
        comeco = L;

        for (numLeg2 = 1; (numLeg - 1 != numLeg2); numLeg2++)
            L = L->prox;

        noAnt = L;
        atual = L->prox;
        L = comeco;

        tempoInicial2 = atual->horasIni * 3600000 + atual->minutosIni * 60000 + atual->segundosIni * 1000 + atual->milissegundosIni;
        tempoFinal2 = noAnt->horasFim * 3600000 + noAnt->minutosFim * 60000 + noAnt->segundosFim * 1000 + noAnt->milissegundosFim;

        if (tempoInicial >= tempoFinal2 && tempoFinal <= tempoInicial2 && tempoInicial < tempoFinal) {
            noAnt->prox = aux;
            aux->prox = atual;
            printf("Legenda inclu�da com sucesso!");
        } else {
            printf("N�o foi poss�vel inserir essa legenda, pois o tempo n�o condiz com o n�mero da legenda.\n");
            free(aux);
        }
    }

    return L;
}

legenda retira(legenda L) {
    int i = 1, v; //v->n�mero da legenda a ser exclu�da.
    legenda aux, exc, noAnt, proxNo;

    printf("\nQual o n�mero da legenda que deseja excluir?\n");
    scanf("%d", &v);

    aux = L;
    for (i = 0; L; i++) {
        L = L->prox;
    }
    L = aux;

    if (!L) {
        printf("\nArquivo sem legendas, imposs�vel remover.\n");
    } else if (v == 1) {
        aux = L;
        L = L->prox;
        free(aux);
    } else if (v > i) {
        printf("\nO arquivo possui uma quantidade menor que %d legendas.\n", v);
    } else {
        aux = L;
        for (i = 1; i != v; i++) {
            noAnt = L;
            L = L->prox;
        }

        exc = L;
        proxNo = L->prox;
        noAnt->prox = proxNo;
        L = aux;
        free(exc);
    }

    return L;
}

void consulta(legenda L) {
    int horas, minutos, segundos, milissegundos, numLeg = 1, contador = 5;
    double tempoInicialNo, tempo;
    legenda comeco;

    printf("Digite a hora do momento inicial da legenda que deseja consultar:\n");
    scanf("%d", &horas);
    printf("Digite os minutos do momento inicial da legenda que deseja consultar:\n");
    scanf("%d", &minutos);
    printf("Digite os segundos do momento inicial da legenda que deseja consultar:\n");
    scanf("%d", &segundos);
    printf("Digite os milissegundos do momento inicial da legenda que deseja consultar:\n");
    scanf("%d", &milissegundos);

    tempo = horas * 3600000 + minutos * 60000 + segundos * 1000 + milissegundos;

    comeco = L;
    while (L) {
        tempoInicialNo = L->horasIni * 3600000 + L->minutosIni * 60000 + L->segundosIni * 1000 + L->milissegundosIni;
        if (tempoInicialNo > tempo) break;
        L = L->prox;
        numLeg++;
    }

    if (!L) {
        printf("\nN�o existem legendas posteriores a esse tempo.\n\n");
    } else {
        while ((L) && (contador > 0)) {
            printf("%d\n", numLeg);
            printf("%.2d:%.2d:%.2d,%.3d --> %.2d:%.2d:%.2d,%.3d\n", L->horasIni, L->minutosIni, L->segundosIni, L->milissegundosIni, L->horasFim, L->minutosFim, L->segundosFim, L->milissegundosFim);
            printf("%s\n\n", L->texto);
            L = L->prox;
            contador--;
            numLeg++;
        }
    }

    L = comeco;
}

legenda atualizaLegenda(legenda L) {

    int numLeg, cont = 1, opcao, hI, mI, sI, mlI, hF, mF, sF, mlF, quantNos;
    legenda comeco, auxA, auxP, copia;
    double tempoI, tempoF, tempoAntF, tempoProxI;
    char leg[1000];
    
    quantNos = contaNos(L);

    puts("\n\nInforme o numero da Legenda a ser atualizada:");
    scanf("%d", &numLeg);

    printf("\nInforme o campo a ser alterado:\n1-Tempo\n2-Texto\n");
    scanf("%d", &opcao);

    while (opcao < 1 || opcao > 2) {
        printf("Op�ao invalida, digite uma opcao valida: ");
        scanf("%d", &opcao);
    }

    if (opcao == 1) {
        comeco = L;
        auxA = L;
        auxP = L->prox;

        //Inserindo Tempo Inicial
        puts("\nInsira o novo tempo:");
        puts("\nTempo Inicial\n");
        printf("\nHoras: ");
        scanf("%d", &hI);
        printf("\nMinutos: ");
        scanf("%d", &mI);
        printf("\nSegundos: ");
        scanf("%d", &sI);
        printf("\nMilisegundos: ");
        scanf("%d", &mlI);
        //Inserindo Tempo FInal
        puts("\n\nTempo Final");
        printf("\nHoras: ");
        scanf("%d", &hF);
        printf("\nMinutos: ");
        scanf("%d", &mF);
        printf("\nSegundos: ");
        scanf("%d", &sF);
        printf("\nMilisegundos: ");
        scanf("%d", &mlF);

        // Transformando tempo em Milisegundos(N� atual)
        tempoI = hI * 3600000 + mI * 60000 + sI * 1000 + mlI;
        tempoF = hF * 3600000 + mF * 60000 + sF * 1000 + mlF;

        while (L && cont < quantNos) {
            if (numLeg == cont) {
                // Transformando tempo em Milisegundos(N� anterior) 
                if(cont > 1) tempoAntF = auxA->horasFim * 3600000 + auxA->minutosFim * 60000 + auxA->segundosFim * 1000 + auxA->segundosFim;
                // Transformando tempo em Milisegundos(N� posterior) 
                if (L->prox) tempoProxI = (auxP->horasIni * 3600000) + (auxP->minutosIni * 60000) + (auxP->segundosIni * 1000) + (auxP->milissegundosIni);

                if (tempoI >= 0 && (((tempoF <= tempoProxI ) && (tempoF > tempoI)) || ((tempoF <= tempoProxI) && (tempoF > tempoI) && (tempoI >= tempoAntF)) || ((tempoI >= tempoAntF) && (L->prox == NULL) && (tempoF > tempoI)))) {

                    L->horasIni = hI;
                    L->minutosIni = mI;
                    L->segundosIni = sI;
                    L->milissegundosIni = mlI;
                    L->horasFim = hF;
                    L->minutosFim = hF;
                    L->segundosFim = sF;
                    L->milissegundosFim = mlF;
                    puts("\nTempo atualizado!");
                    cont = quantNos;

                } else {
                    puts("\nIncoerencia no tempo. Nao foi atualizado!");
                    cont = quantNos;
                }
            }//fim IF
            
            cont++;
            if(cont < quantNos){
                auxA = L;
                L = L->prox;
                if(L->prox) auxP = L->prox;
            }
        
        }//fim While
        L = comeco;
    } else {
        
                copia = L;
        
                while (L && cont < quantNos) {
                    if (numLeg == cont) {
                        puts("\n Insira a nova Legenda:");
                        //scanf(" %s", L->texto);
                        scanf(" %[A-Z a-z 0-9]", L->texto);
                        cont = quantNos;
                    }

                    L = L->prox;
                    cont++;
                }//fim While
                L=copia;
         
    }

    return L;
}

legenda atualizaTempo(legenda L) {

    int numLeg, i = 1, opcao, horas, minutos, segundos, milissegundos;
    double tempoInicial, tempoFinal, horasDec, minutosDec, segundosDec, tempoAnterior;
    legenda comeco;

    printf("\nInforme o numero da legenda onde deseja iniciar a modific�o do tempo: ");
    scanf("%d", &numLeg);
    printf("Digite as horas: ");
    scanf("%d", &horas);
    printf("Digite os minutos: ");
    scanf("%d", &minutos);
    printf("Digite os segundos: ");
    scanf("%d", &segundos);
    printf("Digite os milissegundos: ");
    scanf("%d", &milissegundos);
    double tempo = horas * 3600000 + minutos * 60000 + segundos * 1000 + milissegundos;

    printf("\nA partir da legenda '%d', deseja aumentar ou diminuir o tempo: ", numLeg);
    printf("\n\t1-Aumentar\n\t2-Diminuir\nOpc�o: ");
    scanf("%d", &opcao);

    while (opcao < 1 || opcao > 2) {
        printf("\n\tERRO!!\nOp�ao invalida, digite uma op�ao valida: ");
        scanf("%d", &opcao);
    }

    comeco = L;

    while (L && i < numLeg) {
        if (i >= 2) tempoAnterior = L->horasFim * 3600000 + L->minutosFim * 60000 + L->segundosFim * 1000 + L->milissegundosFim;
        L = L->prox;
        i++;
    }

    while (L) {
        tempoInicial = L->horasIni * 3600000 + L->minutosIni * 60000 + L->segundosIni * 1000 + L->milissegundosIni;
        tempoFinal = L->horasFim * 3600000 + L->minutosFim * 60000 + L->segundosFim * 1000 + L->milissegundosFim;

        if (opcao == 1) {
            tempoInicial += tempo;
            tempoFinal += tempo;
        } else {
            tempoInicial -= tempo;
            tempoFinal -= tempo;
            if (tempoAnterior >= tempoInicial) {
                printf("ERRO! N�o � poss�vel atualizar esse tempo, incosistente com o n�mero da legenda.\n");
                L = comeco;
                return L;
            }
        }

        L->horasIni = tempoInicial / 3600000;
        horasDec = (tempoInicial / 3600000.0) - L->horasIni;
        L->minutosIni = horasDec * 60;
        minutosDec = (horasDec * 60.0) - L->minutosIni;
        L->segundosIni = minutosDec * 60;
        segundosDec = (minutosDec * 60.0) - L->segundosIni;
        L->milissegundosIni = segundosDec * 1000;

        L->horasFim = tempoFinal / 3600000;
        horasDec = (tempoFinal / 3600000.0) - L->horasFim;
        L->minutosFim = horasDec * 60;
        minutosDec = (horasDec * 60.0) - L->minutosFim;
        L->segundosFim = minutosDec * 60;
        segundosDec = (minutosDec * 60.0) - L->segundosFim;
        L->milissegundosFim = segundosDec * 1000;

        L = L->prox;
    }

    L = comeco;

    return L;
}

void printaLista(legenda L, char *saida) {

    int i = 1;
    FILE *arq;
    arq = fopen(saida, "w");

    while (L) {

        fprintf(arq, "%d\n", i);
        fprintf(arq, "%.2d:%.2d:%.2d,%.3d --> %.2d:%.2d:%.2d,%.3d\n", L->horasIni, L->minutosIni, L->segundosIni, L->milissegundosIni, L->horasFim, L->minutosFim, L->segundosFim, L->milissegundosFim);
        fprintf(arq, "%s\n\n", L->texto);

        L = L->prox;
        i++;
    }

    fclose(arq);
}

int contaNos(legenda L){
    
    int i;
    
    while (L) {
        L = L->prox;
        i++;
    }
    
    return i;
    
}
    

int main(int argc, char** argv) {

    char nomeArquivo[200], texto[1000];
    int opcao;

    printf("\t\tMenu\n\nDigite o nome do arquivo de legenda:");
    gets(nomeArquivo);

    legenda L = criaLista(nomeArquivo);

    printf("\nInforme a op��o desejada:\n1-Inserir uma legenda\n2-Remover uma legenda\n3-Consultar Legendas\n4-Atualizar Legenda\n5-Atualizar tempo\n6-Sair do Sistema\n");
    scanf("%d", &opcao);

    while (opcao < 1 || opcao > 6) {
        printf("Op�ao invalida, digite uma op�ao valida: ");
        scanf("%d", &opcao);
    }

    while (opcao != 6) {
        switch (opcao) {
            case(1):
                L = insere(L);
                printaLista(L, nomeArquivo);
                break;
            case(2):
                L = retira(L);
                printaLista(L, nomeArquivo);
                break;
            case(3):
                consulta(L);
                break;
            case(4):
                L = atualizaLegenda(L);
                printaLista(L, nomeArquivo);
                break;
            case(5):
                L = atualizaTempo(L);
                printaLista(L, nomeArquivo);
                break;
            default: printf("\nAdeus\n");
                return 0;
        }
        printf("\nInforme a op��o desejada:\n1-Inserir uma legenda\n2-Remover uma legenda\n3-Consultar Legendas\n4-Atualizar Legenda\n5-Atualizar tempo\n6-Sair do Sistema\n");
        scanf("%d", &opcao);
    }

    return (EXIT_SUCCESS);
}
